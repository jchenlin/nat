/**********************************************************************
 * Students: Jack Chen Lin (g1chenli)
 * 			 Qin Riu Li (g1ll)
 * 			 Ren Liu (c3liuren)
 *
 * file:  sr_router.c
 * date:  Mon Feb 18 12:50:42 PST 2002
 * Contact: casado@stanford.edu
 *
 * Description:
 *
 * This file contains all the functions that interact directly
 * with the routing table, as well as the main entry method
 * for routing.
 *
 **********************************************************************/

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include "sr_if.h"
#include "sr_rt.h"
#include "sr_router.h"
#include "sr_nat.h"
#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_utils.h"


#define NAT_IFACE_NAME "eth2"
/*---------------------------------------------------------------------
 * Method: sr_init(void)
 * Scope:  Global
 *
 * Initialize the routing subsystem
 *
 *---------------------------------------------------------------------*/
int is_arp(uint16_t etype);
int packet_sanity_check(unsigned int len, uint16_t etype, uint8_t * packet);
int is_arp(uint16_t etype);
int is_tcp(uint8_t protocol);
int ethernet_sanity_check(unsigned int len);
int arp_sanity_check(unsigned int len, uint8_t * packet);
int ip_sanity_check(unsigned int len, uint8_t * packet);
int valid_ip_checksum(sr_ip_hdr_t * iphdr, unsigned int len);
int valid_icmp_checksum(sr_icmp_hdr_t * icmp_hdr, unsigned int len);
uint16_t calculate_ip_cksum(sr_ip_hdr_t * iphdr, unsigned int len);
uint16_t calculate_icmp_cksum(sr_icmp_hdr_t * icmphdr, unsigned int len);
int same_ip(uint32_t ip1, uint32_t ip2);
struct sr_rt * longest_prefix_match(uint32_t ip_dst, struct sr_instance *sr);
void sr_handle_icmp(struct sr_instance* sr, uint8_t * packet/* lent */,
	unsigned int len, char* interface/* lent */, struct sr_ethernet_hdr *ehdr, 
	sr_ip_hdr_t *iphdr, sr_icmp_hdr_t * icmphdr);
void sr_handle_arp(struct sr_instance* sr, uint8_t * packet/* lent */, 
	char* interface/* lent */, unsigned int len,struct sr_ethernet_hdr *ehdr);

void sr_init(struct sr_instance* sr)
{
	/* REQUIRES */
	assert(sr);

	/* Initialize cache and cache cleanup thread */
	sr_arpcache_init(&(sr->cache));

	pthread_attr_init(&(sr->attr));
	pthread_attr_setdetachstate(&(sr->attr), PTHREAD_CREATE_JOINABLE);
	pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
	pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
	pthread_t thread;
	printf("Before creating timeout thread\n");
	pthread_create(&thread, &(sr->attr), sr_arpcache_timeout, sr);
	printf("After creating timeout thread\n");

	/* Add initialization code here! */

} /* -- sr_init -- */

/*---------------------------------------------------------------------
 * Method: sr_handlepacket(uint8_t* p,char* interface)
 * Scope:  Global
 *
 * This method is called each time the router receives a packet on the
 * interface.  The packet buffer, the packet length and the receiving
 * interface are passed in as parameters. The packet is complete with
 * ethernet headers.
 *
 * Note: Both the packet buffer and the character's memory are handled
 * by sr_vns_comm.c that means do NOT delete either.  Make a copy of the
 * packet instead if you intend to keep it around beyond the scope of
 * the method call.
 *
 *---------------------------------------------------------------------*/

void sr_handlepacket(struct sr_instance* sr,
		uint8_t * packet/* lent */,
		unsigned int len,
		char* interface/* lent */)
{
	/* REQUIRES */
	assert(sr);
	assert(packet);
	assert(interface);
	struct sr_nat* nat = sr->nat;
	/*set ext ip here since if_list now populated*/
	if (nat && nat->ip_ext == 0){
		assert(nat);
		struct sr_if* iface = sr_get_interface(nat->sr, NAT_IFACE_NAME);
		nat->ip_ext = iface->ip;
		assert(nat->ip_ext);
	}

	printf("*** -> Received packet of length %d \n",len);
	/*get ethernet header and type*/
	struct sr_ethernet_hdr *ehdr = (sr_ethernet_hdr_t*)packet;
	uint16_t etype = ntohs(ehdr->ether_type);
	if (ethernet_sanity_check(len)){
		/* If packet is an IP packet and IP sanity check passes. */
		if(is_ip(etype) && ip_sanity_check(len, (packet + eth_hdr_size))){
			uint8_t *buf = packet + sizeof(sr_ethernet_hdr_t);
			sr_ip_hdr_t *iphdr = (sr_ip_hdr_t *)(buf);
			uint8_t ip = ip_protocol(buf);
			sr_icmp_hdr_t * icmphdr = (packet + eth_hdr_size + ip_hdr_size);

			/* If packet is an ICMP packet and ICMP sanity check passes. */
			if (is_icmp(ip) && icmp_sanity_check(len, icmphdr)){
				if (nat){
					sr_nat_handle_icmp(nat,packet, len, interface,ehdr, iphdr, icmphdr );
				}else{
					sr_handle_icmp(sr, packet, len, interface, ehdr, iphdr, icmphdr);
				}
			} else if(is_tcp(ip) && nat){
				/*sr_tcp_hdr_t * tcphdr = (sr_tcp_hdr_t *)(packet + eth_hdr_size + ip_hdr_size);
				valid_TCP_checksum(tcphdr, sizeof(sr_tcp_hdr_t));*/
				sr_nat_handle_TCP(nat, packet, len, interface, ehdr, iphdr);
			} else { /* Handling IP packet */

				struct sr_if* my_iface = sr_get_interface(sr, interface);
				if (in_our_interfaces(sr, iphdr->ip_dst)) {

					/* UDP/TCP payload*/
					if(ip == 0x06 || ip == 0x11){
						printf("We are sending 3,3\n");
						ICMP_reply(sr, packet, len, interface,ehdr->ether_dhost , iphdr->ip_dst, 3, 3);
						return;
					}


				} else { /* IP not addressed to us; follow normal forwarding logic. */
					/* if(!sr.nat){ sr_nat_handle_TCP(sr.nat, packet, len,interface); }*/ /*in handle_TCP if UDP dump*/
					/* else {}*/

					iphdr->ip_ttl--;

					/* Recompute the packet checksum over the modified header */
					iphdr->ip_sum = calculate_ip_cksum(iphdr, len);
					/* Find longest prefix match in the routing table that matches
					 * with the destination IP address
					 */
					struct sr_rt* lpm = 0;
					lpm = longest_prefix_match(iphdr->ip_dst, sr);
					if (lpm == 0) { /* Error */
						printf("No longest prefix match");
						ICMP_reply(sr, packet, len, interface,ehdr->ether_dhost , iphdr->ip_dst, 3, 0);
						return;
					}

					struct sr_if* lpm_iface = sr_get_interface(sr, lpm->interface);
					if (iphdr->ip_ttl <= 0) { /* TTL = 0: drop packet */
						/* Send ICMP type 11 code 0 */
						ICMP_reply(sr, packet, len, interface, ehdr->ether_dhost , my_iface->ip,(uint8_t)11, 0);
						return;
					}

					/*
					 * Check the ARP cache for next-hop MAC address corresponding
					 * to the next-hop IP.
					 */
					struct sr_arpcache *cache = &(sr->cache);
					struct sr_arpentry *entry = NULL;

					entry = sr_arpcache_lookup(cache, lpm->dest.s_addr);
					if (entry != NULL) { /* Found entry in cache and sending packet.*/

						/* Write destination MAC in ethernet header */
						iphdr->ip_ttl--;
						iphdr->ip_sum = calculate_ip_cksum(iphdr, ip_hdr_size);
						memcpy(ehdr->ether_shost, lpm_iface->addr, ETHER_ADDR_LEN);
						memcpy(ehdr->ether_dhost,  entry->mac, ETHER_ADDR_LEN);

						sr_send_packet(sr, packet, len, lpm->interface);
						free(entry);
						return;
					} else {

						struct sr_arpreq *req;
						/* Queue request to ARP cache table. */
						req = sr_arpcache_queuereq(cache, iphdr->ip_dst, packet,len,lpm_iface->name);
						if (!req) {
							printf("Failed to queue request.\n");
							return;
						}

						handle_arpreq(sr, req);
						return;
					}

					/* Destination Net Unreachable */
					ICMP_reply(sr, packet, len, interface, ehdr->ether_dhost , iphdr->ip_dst, 3, 0);
				}
			}
		} else if (is_arp(etype) && arp_sanity_check(len, packet)){ /* Handle ARP packet. */
			sr_handle_arp( sr, packet, interface, len, ehdr);
		}
	}
} /* end sr_ForwardPacket */

/**
 * Sanity check for arp packets
 */
int arp_sanity_check(unsigned int len, uint8_t * packet){
	int min_arp_size =  sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t);
	uint8_t *new_packet = (packet + sizeof(sr_ethernet_hdr_t));
	sr_arp_hdr_t *arp_hdr = (sr_arp_hdr_t *)(new_packet);
	if (len < min_arp_size){
		printf("ARP packet size failed!\n");
		return 0;
	}else if (arp_hdr->ar_hrd != htons(arp_hrd_ethernet)) {
		printf("ARP hardware format failed!\n");
		return 0;
	}else if (arp_hdr->ar_pro != htons(ethertype_ip)) {
		printf("ARP header protocol address format failed!\n");
		return 0;
	}
	return 1;
}
/**
 * Sanity check for ethernet packets
 */
int ethernet_sanity_check(unsigned int len){
	int min_eth_size = sizeof(sr_ethernet_hdr_t);
	if (len < min_eth_size){
		printf("Ethernet packet size failed!\n");
		return 0;
	}
	return 1;
}

/**
 * Sanity check for IP Packets
 * @param  len    length of packet recieved
 * @param  packet packet receieved
 * @return 0 if failed; 1 otherwise
 */
int ip_sanity_check(unsigned int len, uint8_t * packet){
	int min_ip_size =  sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t);
	if (len < min_ip_size){
		printf("IP packet size failed!\n");
		return 0;
	}
	/*ip version need to be IPV4*/
	sr_ip_hdr_t *iphdr = (sr_ip_hdr_t *)(packet);
	if(iphdr->ip_v != 4){
		printf("Non IPv4 packet!\n");
		return 0;
	}
	if (valid_ip_checksum(iphdr, ip_hdr_size) == 0)
	{
		printf("IP Checksum failed!\n");
		return 0;
	}
	return 1;
}
/**
 * Validates ip's checksum is correct
 */

int valid_ip_checksum(sr_ip_hdr_t * iphdr, unsigned int len){
	uint16_t old_cksum = iphdr->ip_sum;
	if(old_cksum == calculate_ip_cksum(iphdr, len)){

		return 1;
	}else{
		return 0;
	}
}
/**
 * Calculates the checksum over the new ip_header after setting the current checksum to 0, then replace the checksum
 */
uint16_t calculate_ip_cksum(sr_ip_hdr_t * iphdr, unsigned int len){
	uint16_t old_cksum = iphdr->ip_sum;
	iphdr->ip_sum = 0;
	uint16_t new_cksum = cksum(iphdr, len);
	iphdr->ip_sum = old_cksum;
	return new_cksum;
}

/**
 * Sanity check for ICMP Packets
 * @param  len    length of packet recieved
 * @param  packet packet receieved
 * @return 0 if failed; 1 otherwise
 */
int icmp_sanity_check(unsigned int len, uint8_t * packet) {
	int min_icmp_size = sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_hdr_t);
	if (len < min_icmp_size){
		printf("ICMP packet size failed!\n");
		return 0;
	}
	sr_icmp_hdr_t *icmp_hdr = (sr_icmp_hdr_t *)(packet);
	if (valid_icmp_checksum(icmp_hdr, len - (eth_hdr_size + ip_hdr_size)) == 0)
	{
		printf("ICMP Checksum failed!\n");
		return 0;
	}
	return 1;
}

int valid_icmp_checksum(sr_icmp_hdr_t * icmp_hdr, unsigned int len){
	uint16_t old_cksum = icmp_hdr->icmp_sum;
	if(old_cksum == calculate_icmp_cksum(icmp_hdr, len)){
		return 1;
	}else{
		return 0;
	}
}

uint16_t calculate_icmp_cksum(sr_icmp_hdr_t * icmphdr, unsigned int len){
	uint16_t old_cksum = icmphdr->icmp_sum;
	icmphdr->icmp_sum = 0;
	uint16_t new_cksum = cksum(icmphdr, len);
	icmphdr->icmp_sum = old_cksum;
	return new_cksum;
}

int is_arp(uint16_t etype){
	return etype == ethertype_arp;
}

int is_ip(uint16_t etype){
	return etype == ethertype_ip;
}

int is_icmp(uint8_t protocol){
	return protocol == ip_protocol_icmp;
}

int is_tcp(uint8_t protocol){
	return protocol == ip_protocol_tcp;
}

/**
 * Sends an ARP package
 * @param sr          router
 * @param my_iface    interface of router
 * @param ar_tha      ARP target hardware address of package
 * @param ar_tip      ARP target IP of package
 * @param ether_dhost Ethernet destination host of package
 * @param ether_shost Ethernet source host of package
 * @param type        1 for ARP request, 0 for ARP reply
 */
void arp_send(struct sr_instance* sr, struct sr_if* my_iface, 
	unsigned char ar_tha[ETHER_ADDR_LEN],uint32_t ar_tip,
	uint8_t ether_dhost[ETHER_ADDR_LEN], sr_arp_hdr_t *arp_hdr, int type){

	unsigned int reply_packet_size = sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t);
	uint8_t * reply_packet = malloc(reply_packet_size);
	sr_ethernet_hdr_t * new_eth_hdr = (sr_ethernet_hdr_t *) reply_packet;
	sr_arp_hdr_t * new_arp_hdr      = (sr_arp_hdr_t *)(reply_packet + sizeof(sr_ethernet_hdr_t));
	new_eth_hdr->ether_type = htons(ethertype_arp);


	
	/**
	 * Set ARP header to be sent back to sender with appropriate hardware/ip addresss
	 * copy the old header to the new header
	 */
	if (type == 1)
	{
		memset(new_eth_hdr->ether_dhost, ether_dhost, ETHER_ADDR_LEN);
		new_arp_hdr->ar_hln = ETHER_ADDR_LEN;
		new_arp_hdr->ar_hrd = htons(arp_hrd_ethernet);
		new_arp_hdr->ar_pln = 4;
		new_arp_hdr->ar_pro = htons(ethertype_ip);
		new_arp_hdr->ar_op =  htons(arp_op_request); /* ARP opcode (command)*/
		
	}else{
		memcpy(new_arp_hdr, arp_hdr, sizeof(sr_arp_hdr_t));
		memcpy(new_eth_hdr->ether_dhost, ether_dhost, ETHER_ADDR_LEN);
		new_arp_hdr->ar_op =  htons(arp_op_reply); /* ARP opcode (command)*/
	}
	/**
	 * Set ethernet header to be sent back to original source sender from this interface
	 */
	
	memcpy(new_eth_hdr->ether_shost, my_iface->addr, ETHER_ADDR_LEN);
	
	memcpy(new_arp_hdr->ar_tha, ar_tha, ETHER_ADDR_LEN);     /* target hardware address      */
	new_arp_hdr->ar_tip = ar_tip;     /* target IP address            */
	memcpy(new_arp_hdr->ar_sha, my_iface->addr, ETHER_ADDR_LEN);   /* sender hardware address*/
	new_arp_hdr->ar_sip = my_iface->ip;     /* sender IP address            */

	ethernet_sanity_check(reply_packet_size);
	arp_sanity_check( reply_packet_size, reply_packet);

	sr_send_packet(sr, reply_packet, reply_packet_size, my_iface->name);
	free(reply_packet);
}
/**
 * Checks if IP's both have the same print out
 * @param  ip1 IP 1
 * @param  ip2 IP 2
 * @return 1 if ip's are identical, 0 otherwise
 */
int same_ip(uint32_t ip1, uint32_t ip2) {
	uint32_t curOctet1,curOctet2;
	int result = 1;
	int i = 0;
	while(i < 4){
		curOctet1 = (ip1 << 8*i) >> 24;
		curOctet2 = (ip2 << 8*i) >> 24;
		if (curOctet1 != curOctet2){
			result = 0;
		}
		i++;
	}
	return result;
}


/*
 * Check if dest_ip is towards one of our interfaces.
 * Return 1 if it is. Otherwise return 0.
 */
struct sr_if* in_our_interfaces(struct sr_instance* sr, uint32_t dest_ip) {

	struct sr_if* if_walker = 0;
	/* Check interface list */
	assert(sr->if_list);

	if_walker = sr->if_list;
	while(if_walker) {
		if (same_ip(ntohl(if_walker->ip), ntohl(dest_ip))){
			return if_walker;
		}
		if_walker = if_walker->next;
	}
	return 0;
}

/*
 * Get longest prefix match of destination ip in our routing table.
 */
struct sr_rt * longest_prefix_match(uint32_t ip_dst, struct sr_instance *sr) {

	struct sr_rt *my_rt_walker = 0;
	struct sr_rt *longest_rt_walker = 0;
	if(sr->routing_table == 0)
	{
		printf(" *warning* Routing table empty \n");
		return 0;
	}

	my_rt_walker = sr->routing_table;
	uint32_t mask = 0;
	uint32_t gw = 0;
	uint32_t prefix = (uint32_t)0;
	uint32_t lprefix = (uint32_t)0;
	prefix = gw & mask;

	while (my_rt_walker->next) {
		mask = *(uint32_t*) &my_rt_walker->mask;
		gw = *(uint32_t*) &my_rt_walker->gw;
		prefix = gw & mask;

		if (((ip_dst & mask) == (prefix)) && (prefix >= lprefix)) {
			lprefix = prefix;
			longest_rt_walker = my_rt_walker;
		}
		my_rt_walker = my_rt_walker->next;
	}
	mask = *(uint32_t*) &my_rt_walker->mask;
	gw = *(uint32_t*) &my_rt_walker->gw;
	prefix = gw & mask;
	if (((ip_dst & mask) == (prefix)) && (prefix >= lprefix)) {
		longest_rt_walker = my_rt_walker;
	}
	return longest_rt_walker;

}

/*
 * Sends in ICMP reply to the interface of destination (iface_name).
 */
void ICMP_reply(struct sr_instance *sr, uint8_t* packet, unsigned int len,
		char* iface_name, unsigned char ar_sha[ETHER_ADDR_LEN],uint32_t ar_sip,
		uint8_t type, uint8_t code)
{
	/* Create an empty packet */
	uint8_t * reply_packet = malloc(len + sizeof(sr_icmp_t3_hdr_t));
	memcpy(reply_packet, packet, len);
	sr_ethernet_hdr_t * new_eth_hdr = (sr_ethernet_hdr_t *) reply_packet;
	sr_ip_hdr_t * new_ip_hdr = (sr_ip_hdr_t *)(reply_packet + eth_hdr_size);

	sr_icmp_hdr_t *new_icmp_hdr = (sr_icmp_hdr_t*) (reply_packet + eth_hdr_size + ip_hdr_size );

	/*Get original info from packet*/
	sr_ethernet_hdr_t * old_eth_hdr = (sr_ethernet_hdr_t *) packet;
	sr_ip_hdr_t * old_ip_hdr = (sr_ip_hdr_t *)(packet + eth_hdr_size);
	/** Ethernet Header*/
	memcpy(new_eth_hdr->ether_dhost, old_eth_hdr->ether_shost,ETHER_ADDR_LEN);
	memcpy(new_eth_hdr->ether_shost, ar_sha,ETHER_ADDR_LEN);
	new_eth_hdr->ether_type = htons(ethertype_ip);
	/** Too lazy to redeclare, so copy over the IP header from old packet**/
	memcpy(new_ip_hdr, old_ip_hdr, ip_hdr_size);
	new_ip_hdr->ip_ttl = 64;
	new_ip_hdr->ip_p = ip_protocol_icmp;
	new_ip_hdr->ip_sum = 0;

	new_ip_hdr->ip_src = ar_sip;
	new_ip_hdr->ip_dst = old_ip_hdr->ip_src;

	/* Create the ICMP Packet */

	new_icmp_hdr->icmp_type = type;
	new_icmp_hdr->icmp_code = code;
	new_icmp_hdr->icmp_sum = 0;
	/* Check for type 3 (Unreachable) and type 11 (TTL) and copy the packet into ICMP. */
	if (type == 3 || type == 11)
	{
		sr_icmp_t3_hdr_t *new_icmp_t3_hdr = (sr_icmp_t3_hdr_t*) (reply_packet + eth_hdr_size + ip_hdr_size );
		new_icmp_t3_hdr->unused = 0x0;
		new_icmp_t3_hdr->next_mtu = 0;
		memcpy(new_icmp_t3_hdr->data, packet + sizeof(sr_ethernet_hdr_t), new_ip_hdr->ip_hl*4 + 8);/* code */
	}
	new_icmp_hdr->icmp_sum = calculate_icmp_cksum(new_icmp_hdr, len - (eth_hdr_size + ip_hdr_size) );
	new_ip_hdr->ip_sum = calculate_ip_cksum(new_ip_hdr, ip_hdr_size);


	/* ====== End Packet Construction ====== */

	/* Send the packet */
	ethernet_sanity_check( len);
	ip_sanity_check(len,reply_packet +  eth_hdr_size );
	icmp_sanity_check(len,reply_packet + eth_hdr_size + ip_hdr_size );

	if(sr_send_packet(sr, reply_packet, len + sizeof(sr_icmp_t3_hdr_t),  iface_name)){
		printf("Failed in sending\n");
	}

	/* Free the packet */
	free(reply_packet);
}

/**
 * Regular handle ICMPfrom PA1
 */
void sr_handle_icmp(struct sr_instance* sr,
		uint8_t * packet/* lent */,
		unsigned int len,
		char* interface/* lent */,
		struct sr_ethernet_hdr *ehdr,
		sr_ip_hdr_t *iphdr,
		sr_icmp_hdr_t * icmphdr ){
		

	/*check if echo request is for us and reply type 0 code 0.*/
	if (in_our_interfaces(sr, iphdr->ip_dst)) {
		if (icmphdr->icmp_code == 0 && icmphdr->icmp_type == 8){
			ICMP_reply(sr, packet, len, interface, ehdr->ether_dhost , iphdr->ip_dst ,0, 0);
			return;
		}
	}else{
		forward_packet(sr, packet, interface, len, ehdr, iphdr);
	}
}

/**
 * Forward packet if not for us
 */

void forward_packet(struct sr_instance* sr,
		uint8_t * packet/* lent */,
		char* interface/* lent */,
		unsigned int len,
		struct sr_ethernet_hdr *ehdr,
		sr_ip_hdr_t *iphdr){
		sr_icmp_hdr_t * icmphdr = (packet + eth_hdr_size + ip_hdr_size);
		printf("BEGIN PRINTING IP PACKET FOR FORWARD PACKET\n");
		print_hdr_ip(packet + sizeof(sr_ethernet_hdr_t));
		printf("END PRINTING IP PACKET FOR FORWARD PACKET\n" );
		/*forward to next hop*/
		struct sr_rt* lpm = longest_prefix_match(iphdr->ip_dst, sr);
		/* no longest prefix found, only send back error if ping packet*/
		if (lpm == 0 && icmphdr->icmp_code == 0) { /* Error */
			printf("No longest prefix match");
			ICMP_reply(sr, packet, len, interface, ehdr->ether_dhost,iphdr->ip_dst, 3, 0);
			return;
		}
		struct sr_if* lpm_iface = sr_get_interface(sr, lpm->interface);
		struct sr_arpentry * entry = sr_arpcache_lookup(&sr->cache, lpm->dest.s_addr);
		/*next hop found in arp cache table and forward there*/
		if (entry) {
			printf("Found entry ========================\n");
			iphdr->ip_ttl--;
			if (iphdr->ip_ttl <= 0) { /* TTL = 0: drop packet */
				/* Send ICMP type 11 code 0 */
				struct sr_if* my_iface = sr_get_interface(sr, interface);
				ICMP_reply(sr, packet, len, interface, ehdr->ether_dhost , my_iface->ip,(uint8_t)11, 0);
				return;
			}
			iphdr->ip_sum = calculate_ip_cksum(iphdr, ip_hdr_size);
			memcpy(ehdr->ether_shost, lpm_iface->addr, ETHER_ADDR_LEN);
			memcpy(ehdr->ether_dhost,  entry->mac, ETHER_ADDR_LEN);

			sr_send_packet(sr, packet, len, lpm->interface);
			free(entry);
		} else {
			printf("No entry , enqueue ========================\n");
			/* MAC not in table */
			struct sr_arpcache *cache = &(sr->cache);
			/* Queue request to ARP cache table. */
			struct sr_arpreq *req = sr_arpcache_queuereq(cache, iphdr->ip_dst, packet, len, lpm_iface->name);
			if (!req) {
				printf("Failed to queue request.\n");
				return;
			}
			handle_arpreq(sr, req);
		}
}


/**
 * Regular handle ARP from PA1
 */

void sr_handle_arp(struct sr_instance* sr,
		uint8_t * packet/* lent */,
		char* interface/* lent */,
		unsigned int len,
		struct sr_ethernet_hdr *ehdr){
	struct sr_if* my_iface = sr_get_interface(sr, interface);
	uint8_t *new_packet = (packet + sizeof(sr_ethernet_hdr_t));
	sr_arp_hdr_t *arp_hdr = (sr_arp_hdr_t *)(new_packet);
	if (in_our_interfaces(sr, arp_hdr->ar_tip))
	{
		if (ntohs(arp_hdr->ar_op) == arp_op_request){ /* ARP request */
			arp_send(sr, my_iface, arp_hdr->ar_sha ,arp_hdr->ar_sip, ehdr->ether_shost, arp_hdr,0);
		} else if (ntohs(arp_hdr->ar_op) == arp_op_reply){ /* ARP reply */
			printf("ARP REPLY START\n");
			print_hdrs(packet, len);
			printf("ARP REPLY end\n");
			struct sr_arpreq *req = sr_arpcache_insert(&(sr->cache), arp_hdr->ar_sha, arp_hdr->ar_sip);

			/* Send in all packets queued on current request waiting for reply. */
			if (req){
				struct sr_packet *pkt = req->packets;
				while(pkt){
					/**
					 * Change the ethernet header's destination and source hosts and send for all requests in queue
					 */
					sr_ethernet_hdr_t *eth_hdr = (sr_ethernet_hdr_t *)pkt->buf;
					struct sr_if *pkt_iface = sr_get_interface(sr, pkt->iface);
					memcpy(eth_hdr->ether_dhost, arp_hdr->ar_sha, ETHER_ADDR_LEN);
					memcpy(eth_hdr->ether_shost, pkt_iface->addr, ETHER_ADDR_LEN);
					sr_ip_hdr_t *iphdr = (sr_ip_hdr_t *) (pkt->buf + eth_hdr_size);

					iphdr->ip_ttl -= 1;
					iphdr->ip_sum = calculate_ip_cksum(iphdr, ip_hdr_size);
					sr_send_packet(sr, pkt->buf, pkt->len, pkt->iface);
					pkt = pkt->next;

				}
				/* destroy request */
				sr_arpreq_destroy(&(sr->cache), req);
			}
		}
	} else {
		printf("ARP  not addressed to our interfaces\n");
	}
}