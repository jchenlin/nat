#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <assert.h>
#include "sr_nat.h"
#include <unistd.h>
#include "sr_protocol.h"
#include "sr_router.h"
#include "sr_utils.h"
#include "sr_if.h"

#define OUTGOING 1
#define INCOMING 0

static uint16_t generate_port(struct sr_nat_mapping *mappings,
		sr_nat_mapping_type type);
static int Update_TCP_state(struct sr_nat *nat,
		struct sr_nat_mapping *in_out_mapping,
		sr_ip_hdr_t *iphdr, sr_tcp_hdr_t *tcphdr, int in_out);
struct sr_nat_connection *find_connection (struct sr_nat *nat,
		struct sr_nat_mapping *mapping, uint32_t exthost_ip, uint16_t exthost_port);
uint16_t calculate_tcp_cksum(sr_tcp_hdr_t * tcphdr, sr_ip_hdr_t *iphdr);

int valid_icmp_nat_checksum(sr_icmp_nat_hdr_t * icmp_hdr, unsigned int len);
uint16_t calculate_icmp_nat_cksum(sr_icmp_nat_hdr_t * icmphdr, unsigned int len);

int sr_nat_init(struct sr_nat *nat) { /* Initializes the nat */

	assert(nat);

	/* Acquire mutex lock */
	pthread_mutexattr_init(&(nat->attr));
	pthread_mutexattr_settype(&(nat->attr), PTHREAD_MUTEX_RECURSIVE);
	int success = pthread_mutex_init(&(nat->lock), &(nat->attr));

	/* Initialize timeout thread */

	pthread_attr_init(&(nat->thread_attr));
	pthread_attr_setdetachstate(&(nat->thread_attr), PTHREAD_CREATE_JOINABLE);
	pthread_attr_setscope(&(nat->thread_attr), PTHREAD_SCOPE_SYSTEM);
	pthread_attr_setscope(&(nat->thread_attr), PTHREAD_SCOPE_SYSTEM);
	pthread_create(&(nat->thread), &(nat->thread_attr), sr_nat_timeout, nat);

	/* CAREFUL MODIFYING CODE ABOVE THIS LINE! */

	nat->mappings = NULL;
	/* Initialize any variables here */
	nat->ext_if = EXTERNAL_IF; /* external interface */
	nat->int_if = INTERNAL_IF; /* internal interface */

	return success;
}

/*free a single mapping, also used in sr_nat_time_out*/
void sr_nat_free_mapping(struct sr_nat* nat, struct sr_nat_mapping* mapping){
  if (mapping){
    struct sr_nat_mapping *curr, *next = NULL, *prev = NULL;
    /*walk through all mappings if find then point to next*/
    for (curr = nat->mappings; curr != NULL; curr=curr->next){
      if (curr == mapping){ /*mapping found*/
        if (prev){ /*prev is a mapping*/
          next = curr->next;
          prev->next = next;
        } else { /*the first mapping in mappings*/
          next = curr->next;
          nat->mappings = next;
        }
        break;
      }
    }
    /*free all connections*/
    while(mapping->conns){
      struct sr_nat_connection * curr = mapping->conns;
      mapping->conns = curr->next;
      free(curr);
    }
    free(mapping);
  }
}
/*free a connection, multiple conns supports Simultaneous-Open Mode*/
void sr_nat_free_conn(struct sr_nat_mapping* mapping, struct sr_nat_connection* conn){
  if (conn && mapping){
    struct sr_nat_connection *curr, *next = NULL, *prev = NULL;
    /*walk through all conns if find then point to next*/
    for (curr = mapping->conns; curr != NULL; curr=curr->next){
      if (curr == conn){ /*mapping found*/
        if (prev){ /*prev is a mapping*/
          next = curr->next;
          prev->next = next;
        } else { /*the first mapping in mappings*/
          next = curr->next;
          mapping->conns = next;
        }
        break;
      }
    }
    free(conn);
  }
}


int sr_nat_destroy(struct sr_nat *nat) {  /* Destroys the nat (free memory) */

	pthread_mutex_lock(&(nat->lock));
	/* free nat memory here */
  while (nat->mappings){
    /*free all mappings, eventually nat->mappings will be NULL*/
    sr_nat_free_mapping(nat, nat->mappings);
  }
  free(nat);
	pthread_kill(nat->thread, SIGKILL);
	return pthread_mutex_destroy(&(nat->lock)) &&
			pthread_mutexattr_destroy(&(nat->attr));

}

void *sr_nat_timeout(void *nat_ptr) {  /* Periodic Timout handling */
	struct sr_nat *nat = (struct sr_nat *)nat_ptr;
	while (1) {
		sleep(1.0);
		pthread_mutex_lock(&(nat->lock));

		time_t curtime = time(NULL);

		/* handle periodic tasks here */
		/*walk through all mappings. if icmp and timedout, destroy that mapping*/
		/*                           else if tcp and timedout, do following */
    struct sr_nat_mapping *walker = nat->mappings;
    while(walker){
      if(walker->type == nat_mapping_icmp){ /*icmp mapping*/
        if(difftime(curtime, walker->last_updated) > nat->icmp_timeout){ 
          struct sr_nat_mapping *next = walker->next; /*after freed cant find next*/
          printf("ICMP timed out\n");
          sr_nat_free_mapping(nat, walker);
          walker = next;
        } else {
          walker = walker->next;
        }
      } else if(walker->type == nat_mapping_tcp){ /*tcp mapping*/
        struct sr_nat_connection *conn_walker = walker->conns;
        while ( conn_walker ){
        	if( conn_walker->state == ESTABLISHED && 
        		difftime(curtime, conn_walker->accessed) > nat->tcp_estb_timeout)
        	{
        		struct sr_nat_connection *next = conn_walker->next;
        		printf("Tcp connection estb and timed out\n");
        		sr_nat_free_conn(walker, conn_walker);
        		conn_walker = next;
        	} else if(conn_walker->state == (SYN_SENT || FIN_WAIT_1 || TIME_WAIT) &&
        		difftime(curtime, conn_walker->accessed) > nat->tcp_trans_timeout)
        	{
        		struct sr_nat_connection *next = conn_walker->next;
        		printf("Tcp connection estb and timed out\n");
        		sr_nat_free_conn(walker, conn_walker);
        		conn_walker = next;
        	} else if(conn_walker->state == (SYN_RECEIVED ) && 
        		difftime(curtime, conn_walker->accessed) > nat->tcp_trans_timeout)
        	{
        		struct sr_nat_connection *next = conn_walker->next;
        		printf("Tcp simultaneous open timed out\n");
        		/*send icmp 3 3*/
        		sr_nat_free_conn(walker, conn_walker);
        		conn_walker = next;
        	} else {
        		conn_walker = conn_walker->next;
        	}
        	if(walker->conns == NULL){ /*no more active mappings*/
        		struct sr_nat_mapping *next = walker->next;
        		printf("TCP mapping with no connections");
        		sr_nat_free_mapping(nat, walker);
        		walker = next;
        	}
        } 
      } else { /*unkown mapping type*/
        walker = walker->next;
      }
    }
		pthread_mutex_unlock(&(nat->lock));
	}
	return NULL;
}

/* Get the mapping associated with given external port.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_lookup_external(struct sr_nat *nat,
		uint16_t aux_ext, sr_nat_mapping_type type ) {

	pthread_mutex_lock(&(nat->lock));
	/* handle lookup here, malloc and assign to copy */
	struct sr_nat_mapping *copy = NULL;
	struct sr_nat_mapping *nat_mappings = nat->mappings;
	/*traverse mappings looking for the ext and type specified*/
	while(nat_mappings){
		if (nat_mappings->type == type && nat_mappings->aux_ext == aux_ext){
			/*if found, malloc and assign */
			copy = malloc(sizeof(struct sr_nat_mapping));
			memcpy(copy, nat_mappings, sizeof(struct sr_nat_mapping));
			break;
		}
		nat_mappings = nat_mappings->next;
	}
	pthread_mutex_unlock(&(nat->lock));
	return copy;
}

/* Get the mapping associated with given internal (ip, port) pair.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_lookup_internal(struct sr_nat *nat,
		uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type ) {

	pthread_mutex_lock(&(nat->lock));

	/* handle lookup here, malloc and assign to copy. */
	struct sr_nat_mapping *copy = NULL;
	struct sr_nat_mapping *nat_mappings = nat->mappings;
	/*traverse mappings looking for the ext and type specified*/
	while(nat_mappings){
		if (nat_mappings->type == type && nat_mappings->aux_int == aux_int && nat_mappings->ip_int == ip_int ){
			/*if found, malloc and assign */
			copy = malloc(sizeof(struct sr_nat_mapping));
			memcpy(copy, nat_mappings, sizeof(struct sr_nat_mapping));
			break;
		}
		nat_mappings = nat_mappings->next;
	}
	pthread_mutex_unlock(&(nat->lock));
	return copy;
}

/* Insert a new mapping into the nat's mapping table.
   Actually returns a copy to the new mapping, for thread safety.
 */
struct sr_nat_mapping *sr_nat_insert_mapping(struct sr_nat *nat,
		uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type ) {
	printf("INSERTING INTO NAT\n");
	pthread_mutex_lock(&(nat->lock));

	/* handle insert here, create a mapping, and then return a copy of it */
	struct sr_nat_mapping *copy = NULL;
	struct sr_nat_mapping *new_mapping = NULL;
	struct sr_if* iface = sr_get_interface(nat->sr, nat->ext_if);/*nat->ip_ext*/
 	uint16_t aux_ext = generate_port(nat->mappings, type);

	new_mapping = malloc(sizeof(struct sr_nat_mapping));
	copy = malloc(sizeof(struct sr_nat_mapping));
	/* set the mapping attributes*/
	new_mapping->type = type;
	new_mapping->ip_int = ip_int; /* internal ip addr */
	new_mapping->aux_int = aux_int; /* internal port or icmp id */
	new_mapping->ip_ext = iface->ip; /*assign to 0 for now*/
	new_mapping->aux_ext = aux_ext; /*assign to 0 for now */
	new_mapping->last_updated = time(NULL); /* use to timeout mappings, set time to current time*/
	new_mapping->conns = NULL; /* list of connections. null for ICMP */
	/*add it to front of mapping list for simpliplicity*/
	new_mapping->next = nat->mappings;
	nat->mappings = new_mapping;
	assert(nat->mappings);

	memcpy(copy, new_mapping, sizeof(struct sr_nat_mapping));
	pthread_mutex_unlock(&(nat->lock));
	return copy;
}
uint16_t calculate_tcp_cksum(sr_tcp_hdr_t * tcphdr, sr_ip_hdr_t *iphdr){
	uint16_t old_tcp_chksum = tcphdr->chk_sum;
	unsigned int ip_header_len = iphdr->ip_hl * 4;
	unsigned int ip_packet_len = ntohs(iphdr->ip_len);
	unsigned int ip_payload_len = ip_packet_len - ip_header_len;
	/*first construct and place the pseudo header before the tcp*/
	sr_pseudo_tcp_hdr_t * pseudo_tcp = malloc(pseudo_tcp_hdr_size + ip_payload_len);
	pseudo_tcp->src_ip = iphdr->ip_src;
	pseudo_tcp->dst_ip = iphdr->ip_dst;
	pseudo_tcp->zero = 0;
	pseudo_tcp->protocol = 6;
	pseudo_tcp->tcp_len = htons(ip_payload_len);
	/*set to 0 prior to calculation and restore when done*/
	tcphdr->chk_sum = 0;
    memcpy(pseudo_tcp + 1, tcphdr, ip_payload_len);
  
    /*then calculate checksum over both the pseudo header and the TCP*/
    uint16_t calculated_tcp_cksum = cksum(pseudo_tcp, pseudo_tcp_hdr_size + ip_payload_len);
  
    printf("New checksum: %d\n", calculated_tcp_cksum);
    tcphdr->chk_sum = old_tcp_chksum;
    /*discard pseudo header*/
  
    free(pseudo_tcp);
  
    return calculated_tcp_cksum;
}

/* Insert a new mapping into the nat's mapping table.
   Actually returns a copy to the new mapping, for thread safety.
 */
struct sr_nat_connection *sr_nat_insert_conns(struct sr_nat *nat,
		struct sr_nat_mapping *mapping, struct sr_nat_connection *conn,
		sr_nat_mapping_type type ) {

	pthread_mutex_lock(&(nat->lock));

	struct sr_nat_mapping *nat_mappings = nat->mappings;
	struct sr_nat_connection *copy;
	/*traverse mappings looking for the ext and type specified*/
	while(nat_mappings){
		if (nat_mappings->aux_ext == mapping->aux_ext &&
				nat_mappings->aux_int == mapping->aux_ext &&
				nat_mappings->ip_ext == mapping->aux_ext &&
				nat_mappings->ip_int == mapping->ip_int) {
			copy = malloc(sizeof(struct sr_nat_connection));
			conn->next = nat_mappings->conns;
			nat_mappings->conns = conn;
			/* found mappings */
			break;
		}
		nat_mappings = nat_mappings->next;
	}
	memcpy(copy, conn, sizeof(struct sr_nat_connection));
	pthread_mutex_unlock(&(nat->lock));
	return copy;
}
static void print_mappings(struct sr_nat_mapping * mapping){
	printf("PRINT MAPPING START\n");
	printf(" internal ip addr %d \n", mapping->ip_int);
	printf(" external ip addr %d\n", mapping->ip_ext);
	printf(" internal port or icmp id %d \n", mapping->aux_int);
	printf(" external port or icmp id %d\n", mapping->aux_ext);
	
}
/* Assume it's a TCP packet */
void sr_nat_handle_TCP(struct sr_nat *nat,
		uint8_t *packet, unsigned int len,char* interface,
		struct sr_ethernet_hdr *ehdr, sr_ip_hdr_t *iphdr) {

	assert(nat);
	assert(packet);
	assert(interface); /* interface TCP packet is coming from */
	pthread_mutex_lock(&(nat->lock));

	print_hdrs(packet, len);
	sr_tcp_hdr_t * tcphdr = (sr_tcp_hdr_t *)(packet + eth_hdr_size + ip_hdr_size);
	printf("header checksum: %d\n", tcphdr->chk_sum);
	uint16_t calculated = calculate_tcp_cksum(tcphdr, iphdr);
	if (calculated != tcphdr->chk_sum){
		printf("Checksum failed. \n", calculated, tcphdr->chk_sum);
		return; /*drop packet */

	}


	if (interface != nat->int_if) {
		/* Incoming packet from external interface */
		/* Rewrite packet so that its destination is to the client associated
		 * with the NAT mappings. */

		struct sr_nat_mapping *incoming_mapping = NULL;
		incoming_mapping = sr_nat_lookup_external(nat, tcphdr->dst_port,
				nat_mapping_tcp);
		printf("incoming asswer\n");
		assert(nat->mappings);
		print_mappings(nat->mappings);
		/* Check and update tcp state */
		Update_TCP_state(nat, incoming_mapping, iphdr, tcphdr, INCOMING);
		assert(incoming_mapping);
		/* translate packet destination to NAT's designated client */
		tcphdr->dst_port = incoming_mapping->aux_int; /* map to internal port */
		iphdr->ip_dst = incoming_mapping->ip_int; /* map to internal ip */
		/*iphdr->ip_ttl--;*/
		tcphdr->chk_sum = calculate_tcp_cksum(tcphdr, iphdr);
		iphdr->ip_sum = calculate_ip_cksum(iphdr, ip_hdr_size);

		forward_packet(nat->sr, packet, nat->int_if, len, ehdr, iphdr);

	} else if (interface == nat->int_if) {
		
		/* Outgoing packet from internal interface */
		/* Rewrite packet so that its source is NAT's external address */

		struct sr_nat_mapping *outgoing_mapping = NULL;
		outgoing_mapping = sr_nat_lookup_internal(nat, iphdr->ip_src,
				tcphdr->src_port, nat_mapping_tcp);

		/* Check TCP state and handle it accordingly. */
		Update_TCP_state(nat, outgoing_mapping, iphdr, tcphdr, OUTGOING);
		printf("outgoing asswer\n");
		assert(outgoing_mapping);
		/* translate packet source to NAT's outgoing interface */
		tcphdr->src_port = outgoing_mapping->aux_int; /* map to NAT port */
		iphdr->ip_src = outgoing_mapping->ip_ext; /* map to NAT ext ip */
		/*iphdr->ip_ttl--;*/
		tcphdr->chk_sum = calculate_tcp_cksum(tcphdr, iphdr);
		iphdr->ip_sum = calculate_ip_cksum(iphdr, ip_hdr_size);

		/* need to do normal forwarding logic: forward the packet to lpm */
		forward_packet(nat->sr, packet, nat->ext_if, len, ehdr, iphdr);
	} else {
		/* Drop */
	}
	pthread_mutex_unlock(&(nat->lock));

}

static uint16_t generate_port(struct sr_nat_mapping *mappings,
		sr_nat_mapping_type type){
	uint16_t aux_ext;
	struct sr_nat_mapping *walker = NULL;

	/*do we care about different mapping type?*/
  /*if (type == nat_mapping_tcp) {  NAT_TCP_START_PORT }
    else if (type = nat_mapping_icmp) { NAT_ICMP_START_PORT }*/

  aux_ext = NAT_START_PORT;
  walker = mappings;
  while(walker){
      if (walker->aux_ext == htons(aux_ext)){
        aux_ext = (aux_ext+1)%NAT_END_PORT;
        walker = mappings;
      } else {
        walker = walker->next;
      }
  }
  return htons(aux_ext);
}

void sr_nat_handle_icmp(struct sr_nat *nat, uint8_t *packet, unsigned int len, char* interface,
	struct sr_ethernet_hdr *ehdr, sr_ip_hdr_t *iphdr, sr_icmp_hdr_t * icmphdr){
	struct sr_instance* sr = nat->sr;
	struct sr_if *recieved_iface = sr_get_interface(sr, interface);
	struct sr_if *int_iface = sr_get_interface(sr, nat->int_if);
	struct sr_if *ext_iface = sr_get_interface(sr, nat->ext_if);
	/*separate headers*/
	sr_icmp_nat_hdr_t *icmp_nat_hdr = (sr_icmp_nat_hdr_t *)(packet + eth_hdr_size + ip_hdr_size);
	(void) interface;
	assert(nat);
	assert(packet);
	assert(interface);
	printf("IN ICMP\n");
	/* From discussion board: 'router should only respond to ping requests for which the destination IP 
	is same as the IP of the router interface at which the packet is received. i.e internal interface 
	for hosts and external interface for severs.*/
	if (icmphdr->icmp_code == 0 && icmphdr->icmp_type == 8){
		printf("ICMP requests\n");
		if (recieved_iface->ip == iphdr->ip_dst){
			printf("send a reply\n");
			ICMP_reply(sr, packet, len, interface, ehdr->ether_dhost , iphdr->ip_dst ,0, 0);
		}else{
			/*Incoming packet from external interface to internal interface*/
			if (recieved_iface->ip == ext_iface->ip ){
				if(iphdr->ip_dst == int_iface->ip){
					printf("Incoming packet from external interface to internal interface, drop packet\n");
				}else{
					printf("forward\n");
					forward_packet(sr, packet, interface, len, ehdr, iphdr);
				}
				
			}else if(recieved_iface->ip == int_iface->ip){
				printf("forward naty\n");
				forward_nat_packet(sr, packet, interface, len, ehdr, iphdr, icmp_nat_hdr, 1);
			}
		}
		return;
	}else if(icmphdr->icmp_code == 0 && icmphdr->icmp_type == 0){
		/*got reply*/
		printf("Got reply\n");
		print_hdrs(packet, len);
		printf("recieved if\n");
		sr_print_if(recieved_iface);
		if(recieved_iface->ip == ext_iface->ip){
			forward_nat_packet(sr, packet, interface, len, ehdr, iphdr, icmp_nat_hdr, 0);
		}else {
			printf("Outgoing packet from internal interface, find mapping\n" );
			forward_nat_packet(sr, packet, interface, len, ehdr, iphdr, icmp_nat_hdr, 1);
		}
		return;
	}
}

void forward_nat_packet(struct sr_instance* sr,
		uint8_t * packet/* lent */,
		char* interface/* lent */,
		unsigned int len,
		struct sr_ethernet_hdr *ehdr,
		sr_ip_hdr_t *iphdr,
		sr_icmp_nat_hdr_t *icmp_nat_hdr,
		int outgoing
		){

	struct sr_nat_mapping * mapping = NULL;
	struct sr_nat *nat = sr->nat;
	if (outgoing){
		printf("Outgoing packet from internal interface, find mapping\n" );
		mapping = sr_nat_lookup_internal(nat, iphdr->ip_src, icmp_nat_hdr->identf, nat_mapping_icmp);
		if (mapping == NULL){
			/*insert new mapping if nonexistant*/
			mapping = sr_nat_insert_mapping(nat, iphdr->ip_src, icmp_nat_hdr->identf, nat_mapping_icmp);
		}
		/*translate the packet*/
		icmp_nat_hdr->identf = mapping->aux_ext;
		iphdr->ip_src = mapping->ip_ext;
	}else{
		printf("Incoming packet from external interface to internal interface\n");
		mapping = sr_nat_lookup_external(nat, icmp_nat_hdr->identf, nat_mapping_icmp);
		if (mapping == NULL){
			printf("No valid mapping found.\n");
			return;
		}
		/*translate the packet*/
		icmp_nat_hdr->identf = mapping->aux_int;
		iphdr->ip_dst = mapping->ip_int;
	}
	icmp_nat_hdr->icmp_sum = calculate_icmp_nat_cksum(icmp_nat_hdr, len - (eth_hdr_size + ip_hdr_size));
	assert(valid_icmp_nat_checksum(icmp_nat_hdr, len - (eth_hdr_size + ip_hdr_size)));
	free(mapping);
	/*forward*/
	printf("forwarding\n");
	forward_packet(sr, packet,interface, len, ehdr, iphdr);
	
}

/* Update tcp state. Return 0 on success; 1 otherwise. */
static int Update_TCP_state(struct sr_nat *nat,
		struct sr_nat_mapping *in_out_mapping,
		sr_ip_hdr_t *iphdr, sr_tcp_hdr_t *tcphdr, int in_out) {

	uint16_t docb = ntohs(tcphdr->data_off_control_bits);

	if (in_out == INCOMING) {
		/*find_connection(in_out_mapping->conns, in_out_mapping->ip_ext,
				in_out_mapping->aux_ext);*/
		struct sr_nat_mapping *incoming_mapping = in_out_mapping;
		if (incoming_mapping == NULL) {
			printf("INSIDE update tcp state null\n");

		} else {
			struct sr_nat_connection *conn = NULL;
			conn = find_connection(nat, incoming_mapping, iphdr->ip_src,
					tcphdr->src_port);
			if (docb & TCP_SYN) {
				if (conn == NULL) { /* unsolicited syn. Will be dropped after 6 secs. */
					/* create connection */
					conn->accessed = time(NULL);
					conn->exthost_ip = iphdr->ip_src;
					conn->exthost_port = tcphdr->src_port;
					conn->incoming_syn = iphdr;
					conn->next = NULL;
					conn->state = SYN_SENT;
					struct sr_nat_connection *conn_copy = NULL;
					conn_copy = sr_nat_insert_conns(nat, incoming_mapping,
							conn, nat_mapping_tcp);
					if (conn_copy == NULL) {
						return 1; /* insert error */
					}
					in_out_mapping = incoming_mapping ;
					return 0;
				}
			} else if (docb & TCP_ACK && docb & TCP_SYN) {
				if (conn->state == SYN_SENT) {
					conn->state = ESTABLISHED;
					conn->accessed = time(NULL);
				}

			} else if (docb & TCP_ACK) {
				if (conn->state == ESTABLISHED) {
					conn->accessed = time(NULL);
				} else if (conn->state == CLOSE_WAIT) {
					conn->state = LAST_ACK;
					conn->accessed = time(NULL);
				} else if (conn->state == FIN_WAIT_1) {
					conn->state = CLOSING;
					conn->accessed = time(NULL);
				} else if (conn->state == FIN_WAIT_2) {
					conn->state = TIME_WAIT;
					conn->accessed = time(NULL);
				}
			} else if (docb & TCP_PSH) {
				return 1; /* unimplemented */
			} else if (docb & TCP_RST) {
				return 1; /* unimplemented */
			} else if (docb & TCP_URG) {
				return 1; /* unimplemented */
			} else if (docb & TCP_FIN) {
				if (conn->state == ESTABLISHED) {
					conn->state = FIN_WAIT_1;
					conn->accessed = time(NULL);
				} else if(conn->state == FIN_WAIT_1) {
					conn->state = CLOSING;
					conn->accessed = time(NULL);
				}
				in_out_mapping = incoming_mapping ;
				return 0;
			} else if (docb & TCP_FIN && docb & TCP_ACK) {
				if (conn->state == CLOSING) {
					conn->state = TIME_WAIT;
					conn->accessed = time(NULL);
				} else if (conn->state == LAST_ACK) {
					conn->state = CLOSED;
					/* free connection directly */
					sr_nat_free_conn(incoming_mapping, conn);
				}
				in_out_mapping = incoming_mapping ;
				return 0;
			} else return 1;
		}

	} else if (in_out == OUTGOING) {

		struct sr_nat_mapping *outgoing_mapping = in_out_mapping;
		struct sr_nat_connection *conn = NULL;
		if (outgoing_mapping == NULL) { /* no mappings found */
			printf("OUTGOING TCP\n");
			outgoing_mapping = sr_nat_insert_mapping(nat, iphdr->ip_src,
					tcphdr->src_port, nat_mapping_tcp);
			if (docb & TCP_SYN) {
				/* Create new connection. */
				conn->accessed = time(NULL);
				conn->exthost_ip = iphdr->ip_src;
				conn->exthost_port = tcphdr->src_port;
				conn->incoming_syn = iphdr;
				conn->next = NULL;
				conn->state = SYN_SENT;
				struct sr_nat_connection *conn_copy = NULL;
				conn_copy = sr_nat_insert_conns(nat, outgoing_mapping,
						conn, nat_mapping_tcp);
				if (conn_copy == NULL) {
					return 1; /* insert error */
				}
				in_out_mapping = outgoing_mapping ;
				return 0;
			}
			return 1; /* Other TCP messages */
		}


		conn = find_connection(nat, outgoing_mapping, iphdr->ip_dst,
				tcphdr->dst_port);
		if (docb & TCP_SYN) {

		} else if (docb & TCP_ACK && docb & TCP_SYN) {
			if (conn->state == SYN_SENT) {
				conn->state == ESTABLISHED;
			}
			in_out_mapping = outgoing_mapping ;
			return 0;
		} else if (docb & TCP_ACK) {
			if (conn->state == ESTABLISHED) {
				conn->accessed == time(NULL);
			}
			in_out_mapping = outgoing_mapping ;
			return 0;
		} else if (docb & TCP_PSH) {
			return 1; /* unimplemented */
		} else if (docb & TCP_RST) {
			return 1; /* unimplemented */
		} else if (docb & TCP_URG) {
			return 1; /* unimplemented */
		} else if (docb & TCP_FIN && docb & TCP_ACK) {
			if (conn->state == ESTABLISHED) {
				conn->state = CLOSE_WAIT;
				conn->accessed = time(NULL);
			} else if (conn->state == LAST_ACK) {
				conn->state = CLOSED;
				/* free connection directly */
				sr_nat_free_conn(outgoing_mapping, conn);
			} else if (conn->state == FIN_WAIT_1) {
				conn->state = FIN_WAIT_2;
				conn->accessed = time(NULL);
			} else if (conn->state == CLOSING) {
				conn->state = TIME_WAIT;
				conn->accessed = time(NULL);
			}
			in_out_mapping = outgoing_mapping ;
			return 0;
		} else if (docb & TCP_FIN) {
			if (conn->state == ESTABLISHED) {
				conn->state = CLOSE_WAIT;
				conn->accessed = time(NULL);
			} else if (conn->state == FIN_WAIT_1) {
				conn->state = CLOSING;
				conn->accessed = time(NULL);
			} else if (conn->state == FIN_WAIT_2) {
				conn->state = TIME_WAIT;
				conn->accessed = time(NULL);
			}
			in_out_mapping = outgoing_mapping ;
			return 0;
		} else return 1;

	} else {
		return 1; /* misused of function. */
	}
	return 0;
}

/* with lock and mapping has to be mapping has that connection*/
/* given mapping and ip_exthoust & port_exthost Find connection */
struct sr_nat_connection *find_connection (struct sr_nat *nat, struct sr_nat_mapping *mapping,
	uint32_t exthost_ip, uint16_t exthost_port) 
{
	pthread_mutex_lock(&(nat->lock));

	struct sr_nat_connection *copy = NULL;
	struct sr_nat_connection *walker = mapping->conns;
	while(walker){
		if((walker->exthost_ip == exthost_ip) && (walker->exthost_port == exthost_port)){
			walker->accessed = time(NULL);
			copy = (struct sr_nat_connection *) malloc(sizeof(struct sr_nat_connection));
      memcpy(copy, walker, sizeof(struct sr_nat_connection));
			break;
		}
		walker = walker->next;
	}

	pthread_mutex_unlock(&(nat->lock));
	return copy;
}


int valid_icmp_nat_checksum(sr_icmp_nat_hdr_t * icmp_hdr, unsigned int len){
	uint16_t old_cksum = icmp_hdr->icmp_sum;
	if(old_cksum == calculate_icmp_nat_cksum(icmp_hdr, len)){
		return 1;
	}else{
		return 0;
	}
}

uint16_t calculate_icmp_nat_cksum(sr_icmp_nat_hdr_t * icmphdr, unsigned int len){
	uint16_t old_cksum = icmphdr->icmp_sum;
	icmphdr->icmp_sum = 0;
	uint16_t new_cksum = cksum(icmphdr, len);
	icmphdr->icmp_sum = old_cksum;
	return new_cksum;
}
