
#ifndef SR_NAT_TABLE_H
#define SR_NAT_TABLE_H

#include <inttypes.h>
#include <time.h>
#include <pthread.h>
#include "sr_protocol.h"

typedef enum {
  nat_mapping_icmp,
  nat_mapping_tcp
  /* nat_mapping_udp, */
} sr_nat_mapping_type;

/* TCP state */
typedef enum {
	LISTEN,
	SYN_SENT,
	SYN_RECEIVED,
	ESTABLISHED,
	FIN_WAIT_1,
	FIN_WAIT_2,
	CLOSE_WAIT,
	CLOSING,
	LAST_ACK,
	TIME_WAIT,
	CLOSED,
} tcp_state_type;

struct sr_nat_connection {
  /* add TCP connection state data members here */
  tcp_state_type state;
  time_t accessed;
  uint32_t exthost_ip;
  uint16_t exthost_port;
  sr_ip_hdr_t * incoming_syn;

  struct sr_nat_connection *next;
};

struct sr_nat_mapping {
  sr_nat_mapping_type type;
  uint32_t ip_int; /* internal ip addr */
  uint32_t ip_ext; /* external ip addr */
  uint16_t aux_int; /* internal port or icmp id */
  uint16_t aux_ext; /* external port or icmp id */
  time_t last_updated; /* use to timeout mappings */
  struct sr_nat_connection *conns; /* list of connections. null for ICMP */
  struct sr_nat_mapping *next;
};

#define INTERNAL_IF "eth1"
#define EXTERNAL_IF "eth2"
#define NAT_START_PORT 2013
#define NAT_END_PORT 4096



struct sr_nat {
  /* add any fields here */
  struct sr_nat_mapping *mappings;
  struct sr_instance *sr;  

  uint32_t ip_ext; /*out going ip*/

  char * ext_if; /* external interface */
  char * int_if; /* internal interface */

  /*timeouts*/
  int icmp_timeout;
  int tcp_estb_timeout;
  int tcp_trans_timeout;

  /* threading */
  pthread_mutex_t lock;
  pthread_mutexattr_t attr;
  pthread_attr_t thread_attr;
  pthread_t thread;
} sr_nat_t; /*similar to sr_icmp_t3_hdr_t, easy to access size*/


int   sr_nat_init(struct sr_nat *nat);     /* Initializes the nat */
int   sr_nat_destroy(struct sr_nat *nat);  /* Destroys the nat (free memory) */
void *sr_nat_timeout(void *nat_ptr);  /* Periodic Timout */


/* Get the mapping associated with given external port.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_lookup_external(struct sr_nat *nat,
    uint16_t aux_ext, sr_nat_mapping_type type );

/* Get the mapping associated with given internal (ip, port) pair.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_lookup_internal(struct sr_nat *nat,
  uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type );

/* Insert a new mapping into the nat's mapping table.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_insert_mapping(struct sr_nat *nat,
  uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type );

void sr_nat_handle_icmp(struct sr_nat *nat, uint8_t *packet, unsigned int len, char* interface,
  struct sr_ethernet_hdr *ehdr, sr_ip_hdr_t *iphdr, sr_icmp_hdr_t * icmphdr);

void sr_nat_handle_TCP(struct sr_nat *nat, uint8_t *packet, unsigned int len,char* interface,
    struct sr_ethernet_hdr *ehdr, sr_ip_hdr_t *iphdr) ;

int valid_TCP_checksum(sr_tcp_hdr_t * tcphdr, unsigned int len);
#endif
